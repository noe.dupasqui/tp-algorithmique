package s15;

import java.util.Arrays;

public class SubsetSum {
    //============================================================
    public static void main(String[] args) {
        int[] t = {5, 8, 3};
        int max = 20;
        System.out.println(Arrays.toString(t));
        System.out.println("Valid sums < " + max);
        for (int i = 0; i < max; i++) {
            boolean r = subsetSumDyn(t, i);
            if (r)
                System.out.print(i + " ");
        }
    }

    //============================================================
    // Subset Sum Problem : Dynamic Programming version
    //============================================================
    public static boolean subsetSumDyn(int[] elements, int targetSum) {
        boolean[] subsetSumSol = new boolean[targetSum + 1];
        subsetSumSol[0] = true;
        for (int element : elements) {
            for (int i = targetSum-element; i >= 0; i--) {
                if(subsetSumSol[i])
                    subsetSumSol[i+element] = true;
            }
        }
        return subsetSumSol[targetSum];
    }
}
