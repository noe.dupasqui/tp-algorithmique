package s15;

public class GridPath {
    //============================================================
    // Min Path Problem : Recursive version
    //============================================================
    public static int minPath(int[][] t) {
        return minPath(t, t.length - 1, t[0].length - 1);
    }

    public static int minPath(int[][] t, int i, int j) {
        if (i < 0 || j < 0) return Integer.MAX_VALUE / 2;
        if (i == 0 && j == 0) return t[0][0];
        int a = minPath(t, i - 1, j);
        int b = minPath(t, i, j - 1);
        if (b < a) a = b;
        return a + t[i][j];
    }

    //============================================================
    // Min Path Problem : Dynamic Programming version
    //============================================================
    public static int minPathDyn(int[][] grid) {
        int n = grid.length, m = grid[0].length;
        int[][] minPathSol = new int[n][m];

        minPathSol[0][0] = grid[0][0];

        for (int i = 1; i < n; i++) // fill in first row
            minPathSol[i][0] = minPathSol[i - 1][0] + grid[i][0];
        for (int j = 1; j < m; j++) // fill in first column
            minPathSol[0][j] = minPathSol[0][j - 1] + grid[0][j];

        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                int valueLeft = minPathSol[i - 1][j]; // look to the left
                int valueAbove = minPathSol[i][j - 1]; // look above
                minPathSol[i][j] = Math.min(valueLeft, valueAbove) + grid[i][j];
            }
        }
        return minPathSol[n - 1][m - 1];
    }

    //============================================================
    // Small Main
    //============================================================
    public static void main(String[] args) {
        int[][] t = {
                {2, 2, 6, 7},
                {3, 8, 5, 9},
                {2, 1, 8, 2},
                {4, 2, 3, 5}
        };
        System.out.println(minPath(t));
        System.out.println(minPathDyn(t));
    }
}
