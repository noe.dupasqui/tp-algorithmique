package s13;
import java.util.Random;
public class RndTriangle {
  public static void main(String [] args) {
    int nbOfExperiments = 100000;
    Random r = new Random();
    if (args.length>0) nbOfExperiments = Integer.parseInt(args[0]);
    System.out.println(rndTriangleAvgArea(r, nbOfExperiments));
  }
  //============================================================
  public static double rndTriangleAvgArea(Random r, int nbOfExperiments) {
    double result = 0.0;
    for (int i = 0; i < nbOfExperiments; i++) {
      double x1 = r.nextDouble();       //create random double between 0.0-1.0
      double x2 = r.nextDouble();
      double x3 = r.nextDouble();
      double y1 = r.nextDouble();
      double y2 = r.nextDouble();
      double y3 = r.nextDouble();
      result += 0.5*Math.abs(x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2));     //Add the new calculated value
    }
    return result/nbOfExperiments;        //average value of the area
  }
}