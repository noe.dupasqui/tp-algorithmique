package s03;

public class ListItr {
    public final List list;
    public ListNode pred, succ;

    // ----------------------------------------------------------
    public ListItr(List anyList) {
        list = anyList;
        goToFirst();
    }

    // ----------------------------------------------------------
    public void insertAfter(int e) {
        // Creating the new list node
        ListNode newListNode = new ListNode(e, succ, null);

        // Setting the next node as the successor to the new list node
        newListNode.next = succ;

        // Incrementing list size
        list.size++;

        // When there is already a node in the list
        if (succ != null) {
            // When listItr is on the far left
            if (pred == null) {
                succ.prev = newListNode;
                newListNode.next = succ;
                newListNode.prev = null;
                list.first = newListNode;

                // When listItr is between 2 nodes
            } else {
                pred.next = newListNode;
                succ.prev = newListNode;
                newListNode.next = succ;
                newListNode.prev = pred;
            }
            // When listItr is on the far right
        } else if (pred != null) {
            newListNode.prev = pred;
            pred.next = newListNode;
            list.last = newListNode;

            // When there is no node in the list
        } else {
            list.last = newListNode;
            list.first = newListNode;
        }

        // Setting the new node as the successor
        succ = newListNode;

    }

    // ----------------------------------------------------------
    public void removeAfter() {

        // Decrementing the list size
        list.size--;

        // When there is already a node in the list
        if (succ != null) {
            // When listItr is on the far left
            if (pred == null) {
                succ = succ.next;
                if (succ != null) {
                    succ.prev = null;
                }
                list.first = succ;

                // When listItr is between the two last nodes
            } else if (succ.next == null) {
                succ = null;
                pred.next = null;
                list.last = pred;

                // When listItr is between 2 nodes
            } else {
                succ = succ.next;
                succ.prev = pred;
                pred.next = succ;
            }
        }
    }

    // ----------------------------------------------------------
    public int consultAfter() {
        return succ.elt;
    }

    public void goToNext() {
        pred = succ;
        succ = succ.next;
    }

    public void goToPrev() {
        succ = pred;
        pred = pred.prev;
    }

    public void goToFirst() {
        succ = list.first;
        pred = null;
    }

    public void goToLast() {
        pred = list.last;
        succ = null;
    }

    public boolean isFirst() {
        return pred == null;
    }

    public boolean isLast() {
        return succ == null;
    }
}

// When isFirst(), it is forbidden to call goToPrev()
// When isLast(),  it is forbidden to call goToNext() 
// When isLast(),  it is forbidden to call consultAfter(), or removeAfter()
// For an empty list, isLast()==isFirst()==true
// For a fresh ListItr, isFirst()==true
// Using multiple iterators on the same list is allowed only 
//   if none of them modifies the list
