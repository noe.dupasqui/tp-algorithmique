package te1;

import java.util.Locale;

public class Recursivity {
    public static void main(String[] args) {
        System.out.println(isPalindrom("test"));
        System.out.println(isPalindrom("sugus"));
        System.out.println(isPalindrom("SugUs"));

        int n = 7;
        System.out.println(modulo(n, 6));       // 1
        System.out.println(factorial(n));       // 5040
        System.out.println(nbOf1sInBinary(n));  // 3
        System.out.println(fibonacci(n));       // 13
        System.out.println(square(n));          // 49
    }

    public static boolean isPalindrom(String text) {
        text = text.toUpperCase();
        if (text.length() < 2) {
            return true;
        }
        if (text.charAt(0) != text.charAt(text.length() - 1)) {
            return false;
        }
        return isPalindrom(text.substring(1, text.length() - 1));
    }

    // 4 * 3 * 2 * 1
    public static int factorial(int x) {
        if (x == 1) {
            return 1;
        }
        return x * factorial(x - 1);
    }

    // 6 mod 2
    // 2 * 3 + 0
    // 7 mod 2 = 2 * 3 + 1
    public static int modulo(int x, int y) {
        if (x < y) return x;
        return modulo(x - y, y);
    }


    public static int square(int n) {
        if (n == 0) return 0;
        return square(n - 1) + 2 * n - 1;
    }

    public static int nbOf1sInBinary(int x) {
        if (x == 0) return 0;
        return x % 2 + nbOf1sInBinary(x / 2);
    }

    // 0 1 1 2 3 5 8
    public static int fibonacci(int n) {
        if (n < 2) return n;
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
