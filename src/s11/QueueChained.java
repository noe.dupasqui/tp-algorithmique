package s11;

import java.util.Random;

public class QueueChained<E> {
  // ======================================================================
  private static class QueueNode<E> {
    final E elt;
    QueueNode<E> next = null;
    QueueNode(E elt) {
      this.elt = elt;
    }
  }

  // ======================================================================
  private QueueNode<E> queueFront;
  private QueueNode<E> queueBack;

  // ------------------------------
  public QueueChained() {
  }

  // --------------------------
  public void enqueue(E elt) {
    QueueNode<E> queueNode = new QueueNode<E>(elt);

    if (isEmpty()) {
      queueBack = queueNode;
      queueFront = queueNode;
    } else {
      this.queueBack.next = queueNode;
      queueBack = queueNode;
    }

  }

  // --------------------------
  public boolean isEmpty() {
    return queueBack == null;
  }

  // --------------------------
  // PRE : !isEmpty()
  public E consult() {
    return queueFront.elt;
  }

  // --------------------------
  // PRE : !isEmpty()
  public E dequeue() {
    E e = queueFront.elt;
    if (queueFront == queueBack) {
      queueBack = null;
      queueFront = null;
    } else {
      queueFront = queueFront.next;
    }
    return e;
  }

  // --------------------------
  public String toString() {
    String res = "";
    QueueNode<E> c = queueFront;
    while (c != null) {
      res += c.elt + " ";
      c = c.next;
    }
    return res;
  }

  public static void main(String[] args) {
    int n = 10_000, testRuns = 100;
    if (args.length == 1)
      n = Integer.parseInt(args[0]);
    Random r = new Random();
    long seed = r.nextInt(1000);
    r.setSeed(seed);
    System.out.println("Using seed " + seed);
    while (testRuns-- > 0) {
      QueueChained<Integer> q = new QueueChained<>();
      int m = 0;
      int k = 0;
      int p = 0;
      for (int i = 0; i < n; i++) {
        boolean doAdd = r.nextBoolean();
        if (doAdd) {
          k++;
          q.enqueue(k);
          ok(!q.isEmpty(), "should be non-empty " + m + " " + k + " " + p + "\n");
          m++;
          //System.out.print("a("+k+")");
        } else {
          if (m == 0) {
            ok(q.isEmpty(), "should be empty " + m + " " + k + " " + p + "\n");
          } else {
            ok(!q.isEmpty(), "should be non-empty " + m + " " + k + " " + p + "\n");
            int e = q.dequeue();
            //System.out.print("r("+e+")");
            m--;
            ok(e == p + 1, "not FIFO " + m + " " + k + " " + p + "\n");
            p++;
          }
        }
      }
    }
    System.out.println("Test passed successfully");
  }

  //------------------------------------------------------------
  static void ok(boolean b, String s) {
    if (b) return;
    throw new RuntimeException("property not verified: " + s);
  }

}
