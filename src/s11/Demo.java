package s11;
public class Demo {
  static void demo(int n) {
    ObjQueue f;
    int i, sum=0;
    f = new ObjQueue();
    for (i=0; i<n; i++)
      f.enqueue(i);
    while(! f.isEmpty())
      sum = sum + (int) f.dequeue();
    System.out.println(sum);
  }

}

/* Old demo with QueueChained (before ex.03)
public class Demo {
  static void demo(int n) {
    QueueChained<Integer> f;
    int i, sum=0;
    f = new QueueChained<Integer>();
    for (i=0; i<n; i++)
      f.enqueue(i);
    while(! f.isEmpty())
      sum = sum + f.dequeue();
    System.out.println(sum);
  }
}
 */

/* Old demo with IntQueueChained (before ex.03)
public class Demo {
  static void demo(int n) {
    IntQueueChained f;
    int i, sum=0;
    f = new IntQueueChained();
    for (i=0; i<n; i++)
      f.enqueue(i);
    while(! f.isEmpty())
      sum = sum + f.dequeue();
    System.out.println(sum);
  }
}
 */
