package s14;

import java.awt.*;
import java.util.BitSet;

public class SetOfStrings {
    static final int DEFAULT_CAPACITY = 5;
    int crtSize;
    // three "parallel arrays" (alternative: one array HashEntry[],
    // with String/int/boolean fields)
    String[] elt;
    int[] total;
    BitSet busy;

    // ------------------------------------------------------------
    public SetOfStrings() {
        this(DEFAULT_CAPACITY);
    }

    public SetOfStrings(int initialCapacity) {
        initialCapacity = Math.max(2, initialCapacity);
        elt = new String[initialCapacity];
        total = new int[initialCapacity];
        busy = new BitSet(initialCapacity);
        crtSize = 0;
    }

    int capacity() {
        return elt.length;
    }

    // Here is the hash function :
    int hashString(String s) {
        int h = s.hashCode() % capacity();
        if (h < 0) h = -h;
        return h;
    }

    // PRE: table is not full
    // returns the index where element e is stored, or, if
    // absent, the index of an appropriate free cell
    // where e can be stored
    int targetIndex(String e) {
        int valHash = hashString(e);
        int totalValHash = total[valHash];

        do {
            if (busy.get(valHash)) {
                // element found
                if (e.equals(elt[valHash])) {
                    return valHash;
                }

                // Same hash but not the same value
                if (hashString(e) == hashString(elt[valHash])) {
                    totalValHash--;
                }
            }
            valHash++;
            if (valHash >= elt.length) {
                valHash = 0;
            }
        } while (totalValHash > 0);

        // find the next free cell
        valHash = hashString(e);
        while (busy.get(valHash)) {
            valHash++;
            if (valHash >= elt.length) {
                valHash = 0;
            }
        }

        return valHash;
    }

    public void add(String e) {
        if (crtSize * 2 >= capacity())
            doubleTable();

        if (!contains(e)) {
            int indexValue = targetIndex(e);
            total[hashString(e)]++;
            busy.set(indexValue, true);
            elt[indexValue] = e;
            crtSize++;
        }
    }

    private void doubleTable() {
        SetOfStrings newSet = new SetOfStrings(this.elt.length * 2);
        String[] elements = arrayFromSet();

        for (String str : elements)
            newSet.add(str);

        this.busy = newSet.busy;
        this.total = newSet.total;
        this.elt = newSet.elt;
        this.crtSize = newSet.crtSize;
    }

    public void remove(String e) {
        int i = targetIndex(e);
        if (!busy.get(i)) return; // element is absent
        int h = hashString(e);
        total[h]--;
        elt[i] = null;
        busy.clear(i);
        crtSize--;
    }

    public boolean contains(String e) {
        return busy.get(targetIndex(e));
    }

    private class SetOfStringsItr implements s14.SetOfStringsItr {
        final SetOfStrings set;
        int index;

        public SetOfStringsItr(SetOfStrings theSet) {
            this.set = theSet;
        }

        @Override
        public boolean hasMoreElements() {
            return set.busy.nextSetBit(index) != -1;
        }

        @Override
        public String nextElement() {
            index = set.busy.nextSetBit(index);
            return set.elt[index++];
        }
    }

    public SetOfStringsItr iterator() {
        return new SetOfStringsItr(this);
    }

    public void union(SetOfStrings s) {
        String[] fromSet = s.arrayFromSet();

        for (String str : fromSet)
            this.add(str);
    }

    public void intersection(SetOfStrings s) {
        String[] fromSet = s.arrayFromSet();
        SetOfStrings newIntersectionSet = new SetOfStrings();

        for (String str : fromSet) {
            if (this.contains(str))
                newIntersectionSet.add(str);
        }

        this.busy = newIntersectionSet.busy;
        this.total = newIntersectionSet.total;
        this.elt = newIntersectionSet.elt;
        this.crtSize = newIntersectionSet.crtSize;
    }

    public int size() {
        return crtSize;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    private String[] arrayFromSet() {
        String[] t = new String[size()];
        int i = 0;
        SetOfStringsItr itr = this.iterator();
        while (itr.hasMoreElements()) {
            t[i++] = itr.nextElement();
        }
        return t;
    }

    public String toString() {
        SetOfStringsItr itr = this.iterator();
        if (isEmpty()) return "{}";
        String r = "{" + itr.nextElement();
        while (itr.hasMoreElements()) {
            r += ", " + itr.nextElement();
        }
        return r + "}";
    }

    // ------------------------------------------------------------
    // tiny demo
    // ------------------------------------------------------------
    public static void main(String[] args) {
        String a = "abc";
        String b = "defhijk";
        String c = "hahaha";
        SetOfStrings s = new SetOfStrings();
        s.add(a);
        s.add(b);
        s.remove(a);
        if (s.contains(a) || s.contains(c) || !s.contains(b))
            System.out.println("bad news...");
        else
            System.out.println("ok");
    }
}
