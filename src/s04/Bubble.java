package s04;

import s03.List;
import s03.ListItr;


public class Bubble {
    //---------------------------------------------
    static void bubbleSortList(List l) {
        if (l.isEmpty()) return;
        ListItr li = new ListItr(l);
        boolean goOn = true;
        while (goOn) {

            li.goToFirst();
            boolean restart = false; // Show if we have to restart the swapping

            // Go through the list and swap the values if necessary
            while (true) {
                li.goToNext();
                boolean isSwapped = bubbleSwapped(li);

                // If an element has been swapped, we have to go through the list one more time
                if (isSwapped) {
                    restart = true;
                }

                // When we have finished going through the list, we stop the loop and restart if necessary
                if (li.isLast()) {
                    if (!restart) {
                        goOn = false;
                    }
                    break;
                }
            }
        }
    }

    //---------------------------------------------
    //Swaps between left and right element if needed
    //Returns true if swap occurred
    static boolean bubbleSwapped(ListItr li) {
        if (li.isFirst() || li.isLast()) return false;

        if (li.succ.elt < li.pred.elt) {
            // Swapping the elements
            int succValue = li.succ.elt;
            li.succ.elt = li.pred.elt;
            li.pred.elt = succValue;
            return true;
        }
        return false;
    }

    //---------------------------------------------
    public static void main(String[] args) {
        List l = new List();
        ListItr li = new ListItr(l);
        int[] t = {4, 3, 9, 2, 1, 8, 0};
        int[] r = {0, 1, 2, 3, 4, 8, 9};
        for (int i = 0; i < t.length; i++) {
            li.insertAfter(t[i]);
            li.goToNext();
        }
        bubbleSortList(l);
        li = new ListItr(l);
        for (int i = 0; i < r.length; i++) {
            if (li.isLast() || li.consultAfter() != r[i]) {
                System.out.println("Oups... something is wrong");
                System.exit(-1);
            }
            li.goToNext();
        }
        if (!li.isLast()) {
            System.out.println("Oups... too much elements");
            System.exit(-1);
        }
        System.out.println("Test passed successfully");
    }
}