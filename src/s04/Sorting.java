package s04;

public class Sorting {
    public static void main(String[] args) {
        int[] t = {4, 3, 2, 6, 8, 7};
        int[] u = {2, 3, 4, 6, 7, 8};
        insertionSort(t);
        for (int i = 0; i < t.length; i++)
            if (t[i] != u[i]) {
                System.out.println("Something is wrong...");
                return;
            }
        int[] v = {5};
        insertionSort(v);
        int[] w = {};
        insertionSort(w);
        System.out.println("\nMini-test passed successfully...");
    }

    //------------------------------------------------------------
    public static void selectionSort(int[] a) {
        int tmp;

        for (int i = 0; i < a.length; i++) {
            int minIndex = i;
            for (int j = i; j < a.length; j++) {
                if (a[minIndex] > a[j]) {
                    minIndex = j;
                }
            }
            if (a[minIndex] != a[i]) {
                tmp = a[i];
                a[i] = a[minIndex];
                a[minIndex] = tmp;
            }
        }
    }

    //------------------------------------------------------------
    public static void shellSort(int[] a) {
        // Variables' declaration
        int i; // index
        int j; // index for offset
        int v; // value to insert

        int k = 1; // k value
        int firstIndex = 0; // the first index of i

        // Determines the max value of k that will be used for the array
        while (1 + 3 * k < a.length) {
            k = 1 + 3 * k;
        }

        // Determines the first index of i
        while (firstIndex + k < a.length) {
            firstIndex += k;
        }

        // Continues as long as i > 0
        // i <= 0 when k = 1 and when i is the last index of the array
        for (i = firstIndex; i > 0; i++) {
            v = a[i];
            j = i;
            while (j > 0 && a[j - k] > v) {
                a[j] = a[j - k];
                j -= k;
                // If the new index is negative, we have to stop the loop because there are no more value to test
                if ((j - k) < 0) {
                    break;
                }
            }
            // When all values have been checked for the actual k value
            if (i == a.length - 1) {
                // Decrementing the k value with the algorithm
                k = (k - 1) / 3;
                i = k - 1;
            }
            a[j] = v;
        }
    }

    //------------------------------------------------------------
    public static void insertionSort(int[] a) {
        int i, j, v;

        for (i = 1; i < a.length; i++) {
            v = a[i];          // v is the element to insert
            j = i;
            while (j > 0 && a[j - 1] > v) {
                a[j] = a[j - 1];   // move to the right
                j--;
            }
            a[j] = v;          // insert the element
        }
    }
    //------------------------------------------------------------
}
